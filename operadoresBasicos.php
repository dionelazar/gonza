<?php 
//Abrir y cerrar php para indicar que ponemos código php

$miVariable = "Hola mundo";
echo $miVariable; //imprimir pantalla

$var1 = 10;
$var2 = 20;
echo '<br>';
if ($var1 > $var2) 
{
	echo 'var1 es mayor';
}else{
	echo 'var2 es mayor';
}

for ($i=0; $i < 10; $i++) 
{ 
	echo $i;
}

$j = 0;
while ($j <= 10) {
	echo $j;
	$j ++;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Mi primera clase</title>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> por internet-->
	<script src="jquery-3.2.1.min.js"></script></script><!-- esto es para agregar jquery local -->
</head>
<body>
	<br>
	<input id="textoPrueba" type="text">
	<input id="btnPrueba" type="button" value="Enviar" onclick="prueba()">
	<br>
	<input id="num1" type="text">
	<input id="num2" type="text">
	<input id="btnSumar" type="button" value="Sumar" onclick="sumar()">
<script>
	//entre las estiquetas script metemos javascript
	/*$('#btnPrueba').onClick(function(){
		alert('me apretaron');
	});*/

	function prueba()
	{//esto es una función que se llama
		//alert('me apretaron');
		var texto = $('#textoPrueba').val();
		alert(texto);
	}

	function sumar()
	{
		var var1 = $('#num1').val();
		var var2 = $('#num2').val();

		if(Number.isInteger(parseInt(var1)) && Number.isInteger(parseInt(var2)))
		{
			var suma = parseInt(var1) + parseInt(var2);
		}else{
			var suma = var1 + var2;
		}

		alert(suma);
	}

	function myFor()
	{
		for (var i = 0; i < 10; i++) 
		{
			alert(i);
		}
	}

	function myWhile()
	{
		var i = 0;
		while(i < 10)
		{
			console.log(i);
			i = i + 1;
			//i += 1;
			//i++;
		}
	}


</script>
</body>
</html>